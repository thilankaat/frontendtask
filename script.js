$(function() {
  $("#alert").hide(0);
  $("#verifyFormSec").hide();
  $("#registerFormSec").hide();
  // Setup form validation on the #registerform element
  $("#loginForm").validate({
    // Specify the validation rules
    rules: {
      userName: "required"
    },
    // Specify the validation error messages
    messages: {
      userName: "Please enter your first name"
    },

    submitHandler: function(form) {
      $("#btnNext").val("Verifying");
      setTimeout(() => {
        var name = $("#userName").val();
        $("#welcomeName").html(name);
        // form.submit();
        $("#loginFormSec").hide();
        $("#verifyFormSec").show();
        $("#loginForm").trigger("reset");
      }, 500);
    }
  });

  $("#verifyForm").validate({
    // Specify the validation rules
    rules: {
      password: "required"
    },
    // Specify the validation error messages
    messages: {
      password: "Please enter password"
    },

    submitHandler: function(form) {
      //  form.submit();
      $("#verifyForm").trigger("reset");
    }
  });

  $("#passErr").hide();
  $("#unameErr").hide();
  $("#uName").keyup(function() {
    var isValid = validateFields($("#uName").val(), $("#rtuName").val());
    if (isValid) {
      $("#unameErr").hide();
      $("#unameErr").html("*User name and re enter user name should match!");
    } else {
      $("#unameErr").show();
      $("#unameErr").html("*User name and re enter user name should match!");
    }
  });
  $("#rtuName").keyup(function() {
    var isValid = validateFields($("#uName").val(), $("#rtuName").val());
    if (isValid) {
      $("#unameErr").hide();
      $("#unameErr").html("*User name and re enter user name should match!");
    } else {
      $("#unameErr").show();
      $("#unameErr").html("*User name and re enter user name should match!");
    }
  });

  $("#pWord").keyup(function() {
    var isValid = validateFields($("#pWord").val(), $("#rtpWord").val());
    if (isValid) {
      $("#passErr").hide();
      $("#passErr").html("*User name and re enter user name should match!");
    } else {
      $("#passErr").show();
      $("#passErr").html("*User name and re enter user name should match!");
    }
  });
  $("#rtpWord").keyup(function() {
    var isValid = validateFields($("#pWord").val(), $("#rtpWord").val());
    if (isValid) {
      $("#passErr").hide();
      $("#passErr").html("*User name and re enter user name should match!");
    } else {
      $("#passErr").show();
      $("#passErr").html("*User name and re enter user name should match!");
    }
  });

  $("#registryForm").validate({
    // Specify the validation rules
    rules: {
      fName: "required",
      lName: "required",
      rtuName: "required",
      uName: "required",
      pWord: "required",
      rtpWord: "required"
    },
    // Specify the validation error messages
    messages: {
      fName: "Please enter first name",
      lName: "Please enter last name",
      uName: "Please enter user name",
      rtuName: "Please re enter user name",
      pWord: "Please enter password",
      rtpWord: "Please re enter password"
    },

    submitHandler: function(form) {
      //   form.submit();
      if ($("#uName").val() != $("#rtuName").val()) {
        $("#unameErr").show();
        $("#unameErr").html("*User name and re enter user name should match!");
      } else if ($("#pWord").val() != $("#rtpWord").val()) {
        $("#passErr").show();
        $("#passErr").html("*Password and re enter password should match!");
      } else {
        var name = $("#uName").val();
        $("#userName").val(name);
        $("#loginFormSec").show();
        $("#verifyFormSec").hide();
        $("#registerFormSec").hide();
        $("#registryForm").trigger("reset");
        displayAlert("Account created successfully!");
      }
    }
  });
});

function loadRegisterSection() {
  $("#loginFormSec").hide();
  $("#verifyFormSec").hide();
  $("#registerFormSec").show();
  $("#passErr").hide();
  $("#unameErr").hide();
}
function validateFields(field1, field2) {
  if (field1 == field2) {
    return true;
  } else {
    return false;
  }
}
function goBack() {
  $("#loginFormSec").show();
  $("#verifyFormSec").hide();
  $("#btnNext").val("Next");
}
function displayAlert(msg) {
  $("#alert").html(msg);
  $("#alert").show(200);
  setTimeout(() => {
    $("#alert").hide(200);
  }, 2000);
}
